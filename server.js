var http = require('http');

var fs = require('fs');

http.createServer(function (req, res) {
  fs.readFile('data.json', 'utf-8', function(err, data){
    if(err){
      res.writeHead(404, {'Content-Type': 'text/plain'});
      return console.log(err);
    } else {
      res.writeHead(200, {'Content-Type': 'text/plain'});
    }
    res.end(data);
  });
}).listen(1217, '127.0.0.1');

console.log('Server running on 1217');